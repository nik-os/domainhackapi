# Domain Hack API

### Installation

Install the dependencies:

```sh
$ npm install
```

### Configuration

The application configuration is located in the file config.js

```javascript
const config = {
    app: {
        port: parseInt(process.env.PORT) || 3000
    }
};
```

### start the server

```sh
$ npm start
```