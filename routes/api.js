var express = require('express');
var router = express.Router();

var domainManager = require('../managers/domain');

router.post('/domain', function(req, res, next) {
    let domainsList = domainManager.parseDomain(req.body.input);
    const status = domainsList.length? 200 : 204;
    res.status(status).json({
        message: 'Success',
        obj: domainsList
    });
});

module.exports = router;
