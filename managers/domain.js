var fs = require('fs');

var domainsArr = [];

function init() {
    fs.readFile('domains.txt', { encoding : 'utf8' },
        (err, data) => {
            if (err) {
                throw err;
            }
            domainsArr = data.toLowerCase().split('\n');
        })
}

function parseDomain(str) {
    let result = [];
    str = str.toLowerCase();
    if (str.length > 2) {
        domainsArr.forEach(domain => {
            if (str.indexOf(domain) > 0) {
                result.push({
                    str: str.replace(domain, '<span class="domain">.'+domain+'/</span>'),
                    desc: 'Domain description'
                });
            }
        });
    }
    return result;
}

module.exports = {
    init: init,
    parseDomain: parseDomain
}