const config = {
    app: {
        port: parseInt(process.env.PORT) || 3000
    }
};

module.exports = config;